from django.db import models
from amp.sandbox.models import Sandbox


class AMPTask(models.Model):
    """
            Task for deploying VMs.
        Each VM deployed independently. Number of VMs depends on Template.
        Parent task - responsible for Sandbox deployment.
        Child tasks responsible for VMs deployment
    """
    task_id = models.CharField(blank=False, max_length=30)
    parent = models.ForeignKey("self", related_name="parent_task", null=True)
    sandbox = models.ForeignKey(Sandbox, related_name="task", null=True)
