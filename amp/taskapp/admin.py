from django.contrib import  admin
from models import AMPTask

class AMPTaskAdmin(admin.ModelAdmin):
    model = AMPTask


admin.site.register(AMPTask, AMPTaskAdmin)