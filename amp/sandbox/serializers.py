from rest_framework import serializers
from .models import VM, Template, Sandbox
from django.contrib.auth import get_user_model


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = ["name", "note", "ans_playbook"]


User = get_user_model()
class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ["username", "email"]


class SandboxSerializer(serializers.ModelSerializer):
    template = TemplateSerializer(required=True)
    owner = UserSerializer(required=True)
    vms = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Sandbox
        fields = ["name", "template", "owner", "vms", "status"]


class VMSerializer(serializers.ModelSerializer):
    sandbox = SandboxSerializer(required=True)

    class Meta:
        model = VM
        fields = ["hostname", "ip_address", "role", "login", "password"]
