from django.db import models
from django.contrib.auth import get_user_model
from django.conf import settings
# Create your models here.


class VM(models.Model):
    MANAGE_NODE, DATABASE, ENDPOINT = ("manage", 'database', "endpoint")
    VM_ROLES = (
        (MANAGE_NODE, "Manage Node"),
        (DATABASE, "Database"),
        (ENDPOINT, "Endpoint")
    )
    hostname = models.CharField(blank=False, max_length=120)
    ip_address = models.GenericIPAddressField(blank=False)
    sandbox = models.ForeignKey('Sandbox', blank=False, related_name="vms")
    role = models.CharField(choices=VM_ROLES, max_length=30)
    login = models.CharField(blank=False, max_length=30)
    password = models.CharField(blank=True, max_length=30)

    def __unicode__(self):
        return "VM %s %s" %(self.hostname, self.role)


class Sandbox(models.Model):

    READY, INSTALLING, STOPPED, DESTROYED = ("ready", "installing", "stopped", "destroyed")
    SANDBOX_STATUSES = (
        (READY, "READY"),
        (INSTALLING, "INSTALLING"),
        (STOPPED, "STOPPED"),
        (DESTROYED, "DESTROYED")
    )
    name = models.CharField(max_length=90, blank=True, default="NoName")
    template = models.ForeignKey('Template', related_name="sandboxes")
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="sandboxes")
    status = models.CharField(choices=SANDBOX_STATUSES, max_length=20)

    def __unicode__(self):
        return "Sandbox#%s from %s of %s" % (self.id, self.template, self.owner)

    def deploy(self):
        pass

    def destroy(self):
        pass

    def stop(self):
        pass

    def start(self):
        pass


class Template(models.Model):
    ans_playbook = models.CharField(max_length=50, blank=False)
    name = models.CharField(blank=False, max_length=30, help_text="POA/PBA versions information")
    note = models.TextField()

    def __unicode__(self):
        return "Template %s" % self.name
