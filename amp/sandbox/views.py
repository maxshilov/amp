from django.shortcuts import render
import models
# Create your views here.
from django.views.generic import ListView, View
from rest_framework import generics, permissions
from .models import Template, Sandbox, VM
from serializers import SandboxSerializer, VMSerializer



class SandboxListView(ListView):
    model = models.Sandbox
    context_object_name = "envs"
    template_name = "sandbox/dashboard.html"
    paginate_by = 6

    def get_queryset(self):
        qs = models.Sandbox.objects.filter(owner=self.request.user)
        return qs

    def get_context_data(self, **kwargs):
        context = super(SandboxListView, self).get_context_data(**kwargs)
        context['vm_templates'] = models.Template.objects.all()
        return context


class SandboxCreateView(View):
    template_name = "sandbox/dashboard.html"

    def get(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        pass


class SandboxAPICreateView(generics.ListCreateAPIView):
    model = Sandbox
    serializer_class = SandboxSerializer
    permission_classes = [permissions.AllowAny]


class VMAPIListView(generics.ListAPIView):
    model = VM
    serializer_class = VMSerializer
    permission_classes = [permissions.AllowAny]


class VMAPIDetail(generics.RetrieveAPIView):
    model = VM
    serializer_class = VMSerializer





