from django.contrib import admin
from models import Sandbox, Template, VM
# Register your models here.


class VMInline(admin.TabularInline):
    model = VM
    extra = 2


class SandboxAdmin(admin.ModelAdmin):
    model = Sandbox
    inlines = [VMInline]

class TemplateAdmin(admin.ModelAdmin):
    model = Template


class VMAdmin(admin.ModelAdmin):
    model = VM

admin.site.register(Sandbox, SandboxAdmin)
admin.site.register(Template, TemplateAdmin)
admin.site.register(VM, VMAdmin)