from django.conf.urls import url, patterns, include
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
import views


api_urlpatterns = patterns('',
    url(r'^sandbox', views.SandboxAPICreateView.as_view(), name="api-create-sandbox"),
    url(r'^vm', views.VMAPIDetail.as_view(), name="api-get-vm"),
)

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=login_required(views.SandboxListView.as_view()),
        name='list_sandboxes'
    ),
    url(
        regex=r'create',
        view=login_required(views.SandboxCreateView.as_view()),
        name='create'
    ),
    url(r'^api/', include(api_urlpatterns))
]
